FoorumApp.controller('ShowMessageController', function($scope, $routeParams, $location, $route, Api){
    
    Api.getMessage($routeParams.id).success(function(message){
            $scope.message = message;
    });
    
    $scope.newReply = {};
    
    $scope.addReply = function() {
        Api.addReply($scope.newReply, $scope.message.id).success(function(reply) {
            $scope.message.Replies.push(reply); 
            
    });
        
    };
    

});
