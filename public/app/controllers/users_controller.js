FoorumApp.controller('UsersController', function($scope, $location, Api){
  // Toteuta kontrolleri tähän
  $scope.users ={};
  $scope.error = false;
  
  $scope.login = function(){
      Api.login($scope.users).success(function(users){
          $location.path("/");
          console.log('Kirjautuminen onnistui!');
      }).error(function(){
          $scope.error = true;
          console.log('Kirjautuminen epäonnistui');
      });
  };
  
  $scope.newUser = {};
  $scope.passwordError = false;
  $scope.passw;
  $scope.passw2;
  
  $scope.register = function(){
      $scope.error = false;
      $scope.passwordError = false;
      
      //verrataan onko käyttäjän syöyttämät salasanat samat.
      if($scope.passw == $scope.passw2){
          $scope.newUser.password = $scope.passw;
          Api.register($scope.newUser).success(function(users){
              $location.path("/");
              console.log('Rekisteröityminen onnistui')
          }).error(function(){
              $scope.error = true;
          });
      
  }else{
      $scope.passwordError = true;
      console.log('Rekisteroityminen ei onnistu');
  }
  };
});
