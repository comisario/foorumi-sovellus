FoorumApp.controller('ShowTopicController', function($scope, $routeParams, $location, Api){
    
    Api.getTopic($routeParams.id).success(function(topic){
       $scope.topic = topic;
    });
    
    $scope.newMessage = {};
    
    $scope.addMessage = function() {
        console.log($scope.newMessage);
        Api.addMessage($scope.newMessage, $scope.topic.id).success(function(message) {
            $location.path("/messages/" + message.id);
    })};

});
